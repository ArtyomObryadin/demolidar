﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CreateCustomer : MonoBehaviour
{
    private string _nameFromInput;
    private ListOfCustomers ListOfCustomers { get; set; }
    [SerializeField] private TMP_InputField inputField;

    public void OnButtonClick()
    {
        AddCustomer();
    }
    void AddCustomer()
    {
        ListOfCustomers = new ListOfCustomers();
        ListOfCustomers.Customers = new List<CustomerModel>();

        _nameFromInput = inputField.text;
        inputField.text = "";

        ListOfCustomers = SaveManager.LoadData<ListOfCustomers>();

        if (ListOfCustomers == null)
            ListOfCustomers = new ListOfCustomers();
        if (ListOfCustomers.Customers == null)
            ListOfCustomers.Customers = new List<CustomerModel>();

        CustomerModel newCustomer = new CustomerModel();
        newCustomer.Id = ListOfCustomers.Customers.Count;
        newCustomer.Name = _nameFromInput;

        ListOfCustomers.Customers.Add(newCustomer);

        SaveManager.WriteData(ListOfCustomers);
    }
}
