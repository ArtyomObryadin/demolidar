﻿using UnityEngine;
using System.IO;
using System;

public class SaveManager
{
    public static T LoadData<T>(string customPrefix = null)
    {
        T result = default(T);
        var p = GetPath(typeof(T), customPrefix);
        if (File.Exists(p))
        {
            result = JsonUtility.FromJson<T>(File.ReadAllText(p));
        }
        return result;
    }
    public static void WriteData<T>(T data, string customPrefix = null)
    {
        File.WriteAllText(GetPath(typeof(T), customPrefix), JsonUtility.ToJson(data));
    }

    private static string GetPath(Type type, string customPrefix)
    {
#if UNITY_EDITOR
        return Path.Combine(Application.dataPath, $"{type.ToString()}{customPrefix ?? ""}.json");
#else
        return Path.Combine(Application.persistentDataPath, $"{type.ToString()}{customPrefix ?? ""}.json");
#endif

    }
}
