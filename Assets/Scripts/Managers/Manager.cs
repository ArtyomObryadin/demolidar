﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wear.demoLiDAR.Managers
{
    public class Manager : MonoBehaviour
    {

        static Manager instance;

        public static UIManager UIManager;
        public static DotsManager DotsManager;
        public static SaveManager SaveManager;

        public static Manager Instance { get { return instance; } }

        void Awake()
        {
            if (instance == null) { instance = GetComponent<Manager>(); }
            UIManager = GetComponentInChildren<UIManager>();
            DotsManager = GetComponentInChildren<DotsManager>();
        }
    }
}
