﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Wear.demoLiDAR.Managers;

public class UIManager : MonoBehaviour
{
    public void LoadFaceTrackingScene()
    {
        SceneManager.LoadScene("FaceTrackingScene");
    }
    public void UndoButton()
    {
        //undo
        Manager.DotsManager.UndoLastDot();
    }
    public void SelectCustomerButton()
    {
        //openpanel to select customer
    }
    public void CreateCustomerButton()
    {
        //openpanel to create customer
    }
    public void SaveButton()
    {
        //save
        Manager.DotsManager.SaveDots();
        SceneManager.LoadScene("MainMenuScene");
    }
}
