﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class DotsManager : MonoBehaviour
{
    [SerializeField] private Camera arCamera;
    [SerializeField] private GameObject prefabObject;

    public ListOfCustomersData ListOfCustomersData = new ListOfCustomersData();
    public int currentCustomerIndex = 0;

    private CustomerData customerDots;

    bool dotsIsSpawned;
    int lastDotIndex;
    GameObject lastDot;

    private void Start()
    {
        dotsIsSpawned = false;
        if (PlayerPrefs.HasKey("CurrentCustomerId"))
        {
            currentCustomerIndex = PlayerPrefs.GetInt("CurrentCustomerId");
        }
        else
        {
            currentCustomerIndex = 0;
        }

        ListOfCustomersData = SaveManager.LoadData<ListOfCustomersData>();
        if (ListOfCustomersData == null)
            ListOfCustomersData = new ListOfCustomersData();
        if (ListOfCustomersData.CustomersData == null)
            ListOfCustomersData.CustomersData = new List<CustomerData>();
        customerDots = new CustomerData();

        if (currentCustomerIndex > ListOfCustomersData.CustomersData.Count - 1) return;

        customerDots = ListOfCustomersData.CustomersData[currentCustomerIndex];

        if (customerDots == null) return;

        LoadDots();
    }
    void LoadDots()
    {
        var ARFaceCurrent = FindObjectOfType<ARFace>();
        if (ARFaceCurrent == null) return;

        foreach (var dot in customerDots.Dots)
        {
            var currentTransform = dot;
            GameObject newDot = Instantiate(prefabObject, ARFaceCurrent.gameObject.transform);
            newDot.transform.localPosition = currentTransform.ToVector3();
        }
        dotsIsSpawned = true;
    }

    public void UndoLastDot()
    {
        if (lastDot != null)
        {
            Destroy(lastDot);
            customerDots.Dots.RemoveAt(lastDotIndex);
        }
    }
    public void SaveDots()
    {
        if (ListOfCustomersData.CustomersData.Count - 1 >= currentCustomerIndex)
        {
            ListOfCustomersData.CustomersData[currentCustomerIndex] = customerDots;
        }
        else
        {
            ListOfCustomersData.CustomersData.Add(customerDots);
        }
        SaveManager.WriteData(ListOfCustomersData);
    }
    void Update()
    {
        for (int i = 0; i < Input.touchCount; i++)
        {
            var touch = Input.GetTouch(i);
            var touchPhase = touch.phase;
            if (touchPhase == TouchPhase.Began)
            {
                var ray = arCamera.ScreenPointToRay(touch.position);
                var hasHit = Physics.Raycast(ray, out var hit, float.PositiveInfinity);
                if (hasHit)
                    DotsFabric(hit);
            }
        }
        if (!dotsIsSpawned)
            LoadDots();
    }
    void DotsFabric(RaycastHit hit)
    {
        if (hit.transform.GetComponent<ARFace>())
        {
            Quaternion newObjectRotation = Quaternion.FromToRotation(Vector3.up, hit.normal);
            GameObject newObject = Instantiate(prefabObject, hit.point, newObjectRotation);
            var ARFaceCurrent = FindObjectOfType<ARFace>().gameObject;
            newObject.transform.SetParent(ARFaceCurrent.transform);
            lastDot = newObject;
            if (customerDots == null)
                customerDots = new CustomerData();
            if (customerDots.Dots == null)
                customerDots.Dots = new List<SerializableVector3>();
            customerDots.Dots.Add(new SerializableVector3(newObject.transform.localPosition));
            lastDotIndex = customerDots.Dots.Count - 1;
        }

    }
}
