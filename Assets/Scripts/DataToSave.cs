﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DataToSave : MonoBehaviour
{
    public List<List<float[]>> AllCustomerDotsTransform = new List<List<float[]>>();
    public List<string[]> CustomerData = new List<string[]>();
}
