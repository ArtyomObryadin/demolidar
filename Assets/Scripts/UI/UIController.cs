﻿using UnityEngine;
using Wear.demoLiDAR.Managers;

public class UIController : MonoBehaviour
{
    [SerializeField] private CustomerSelectPanel _prefabCustomer;
    [SerializeField] private GameObject _contentController;
    public ListOfCustomers ListOfCustomers = new ListOfCustomers();
    private void OnEnable()
    {
        ClearCustomerButtons();
        CreateCustomerButtons();
    }
    void ClearCustomerButtons()
    {
        var listCustomerButtons = _contentController.GetComponentsInChildren<CustomerSelectPanel>();
        foreach(var CustomerButton in listCustomerButtons)
        {
            Destroy(CustomerButton.gameObject);
        }
    }
    void CreateCustomerButtons()
    {
        ListOfCustomers = SaveManager.LoadData<ListOfCustomers>();
        if (ListOfCustomers == null) return;
        foreach (var customer in ListOfCustomers.Customers)
        {
            var prefabCustomer = Instantiate(_prefabCustomer, _contentController.transform);
            prefabCustomer.Init(customer, ChangeScene);
        }
    }

    void ChangeScene(CustomerModel user)
    {
        PlayerPrefs.SetInt("CurrentCustomerId", user.Id);
        Manager.UIManager.LoadFaceTrackingScene();
    }
}
