﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class CustomerSelectPanel : MonoBehaviour
{
    public CustomerModel Customer { get; set; }
    private Action<CustomerModel> _onClick;

    [SerializeField] private TextMeshProUGUI label;

    public void Init(CustomerModel customer, Action<CustomerModel> onClick)
    {
        Customer = customer;
        _onClick = onClick;
        label.text = customer.Name;

    }

    public void OnButtonClick()
    {
        _onClick?.Invoke(Customer);
    }
}
