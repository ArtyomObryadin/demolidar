﻿using System;
using System.Collections.Generic;

[Serializable]
public class ListOfCustomers
{
    public List<CustomerModel> Customers;
}
