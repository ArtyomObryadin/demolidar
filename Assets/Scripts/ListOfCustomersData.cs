﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ListOfCustomersData
{
    public List<CustomerData> CustomersData;
}

[Serializable]
public class CustomerData
{
    public List<SerializableVector3> Dots;
}


[Serializable]
public class SerializableVector3
{
    public float X;
    public float Y;
    public float Z;

    public SerializableVector3 (Vector3 position)
    {
        X = position.x;
        Y = position.y;
        Z = position.z;
    }

    public Vector3 ToVector3()
    {
        return new Vector3(X, Y, Z);
    }
}
